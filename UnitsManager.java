
// UnitsManager - container for all units

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

class UnitsManager {

    static UnitsManager manager = null;

    ArrayList<Tank> tanks;
    Tank userTank;
    ArrayList<Missile> missiles;
    ArrayList<Wall> walls;

    private UnitsManager() {
        tanks = new ArrayList<Tank>();
        missiles = new ArrayList<Missile>();
        walls = new ArrayList<Wall>();
    }

    public Unit getObstacle(MovingUnit unit, Point p) {
        if (unit instanceof Tank) {
            Tank t = (Tank)unit;
            Rectangle r = new Rectangle(p.x - t.getWidth() / 2, p.y - t.getHeight() / 2, t.getWidth(), t.getHeight());
            for (Iterator i = tanks.iterator(); i.hasNext(); ) {
                t = (Tank)i.next();
                if (t == unit) continue;
                Point tp = t.getPosition();
                if (r.intersects(new Rectangle(tp.x - t.getWidth() / 2, tp.y - t.getHeight() / 2, t.getWidth(), t.getHeight()))) return t;
            }
            for (Iterator i = walls.iterator(); i.hasNext(); ) {
                Wall w = (Wall)i.next();
                Point tp = w.getPosition();
                if (r.intersects(new Rectangle(tp.x - w.getWidth() / 2, tp.y - w.getHeight() / 2, w.getWidth(), w.getHeight()))) return w;
            }
        } else if (unit instanceof Missile) {
            Missile m = (Missile)unit;
            Rectangle r = new Rectangle(p.x - m.getWidth() / 2, p.y - m.getHeight() / 2, m.getWidth(), m.getHeight());
            for (Iterator i = tanks.iterator(); i.hasNext(); ) {
                Tank t = (Tank)i.next();
                if (t == ((Missile)unit).owner) continue;
                Point tp = t.getPosition();
                if (r.intersects(new Rectangle(tp.x - t.getWidth() / 2, tp.y - t.getHeight() / 2, t.getWidth(), t.getHeight()))) return t;
            }
            for (Iterator i = walls.iterator(); i.hasNext(); ) {
                Wall w = (Wall)i.next();
                Point tp = w.getPosition();
                if (r.intersects(new Rectangle(tp.x - w.getWidth() / 2, tp.y - w.getHeight() / 2, w.getWidth(), w.getHeight()))) return w;
            }
        }
        return null;
    }

    public void paint(Graphics g) {
        if (!missiles.isEmpty())
            for (Iterator i = missiles.iterator(); i.hasNext(); ) {
                Missile missile = (Missile)i.next();
                if (missile.isDestroyed()) i.remove();
                else missile.paint(g);
            }
        if (!walls.isEmpty())
            for (Iterator i = walls.iterator(); i.hasNext(); ) {
                Wall wall = (Wall)i.next();
                if (wall.isDestroyed()) {
                    MapManager.getInstance().removeWall(wall.getPosition());
                    i.remove();
                } else wall.paint(g);
            }
        if (!tanks.isEmpty())
            for (Iterator i = tanks.iterator(); i.hasNext(); ) {
                Tank tank = (Tank)i.next();
                if (tank.isDestroyed()) {
		  i.remove();
		  System.out.println("Tank removed");
		} else tank.paint(g);
            }
        if (userTank.isDestroyed()) {
            userTank = null;
            Game.getInstance().finish(false);
        } else if (tanks.size() == 1) {
            Game.getInstance().finish(true);
	}
    }

    public void play() {
        if (!Game.getInstance().isFinished()) {
            MapManager.getInstance().generatePath(userTank.getPosition());
            if (!missiles.isEmpty())
                for (Iterator i = missiles.iterator(); i.hasNext(); ) ((Missile)i.next()).play();
            if (!tanks.isEmpty())
                for (Iterator i = tanks.iterator(); i.hasNext(); ) ((Tank)i.next()).play();
        }
    }

    public void processKeyPress(int key) {
        switch (key) {
            case KeyEvent.VK_UP:
                userTank.setDirection(0);
                userTank.setMoving(true);
                break;
            case KeyEvent.VK_LEFT:
                userTank.setDirection(1);
                userTank.setMoving(true);
                break;
            case KeyEvent.VK_DOWN:
                userTank.setDirection(2);
                userTank.setMoving(true);
                break;
            case KeyEvent.VK_RIGHT:
                userTank.setDirection(3);
                userTank.setMoving(true);
                break;
            case KeyEvent.VK_CONTROL:
                userTank.setShooting(true);
        }
    }

    public void processKeyRelease(int key) {
        switch (key) {
            case KeyEvent.VK_UP:
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_RIGHT:
                userTank.setMoving(false);
                break;
            case KeyEvent.VK_CONTROL:
                userTank.setShooting(false);
        }
    }

    public void pause() {
        if (userTank != null) {
            userTank.setMoving(false);
            userTank.setShooting(false);
        }
    }

    public void stop() {
        userTank = null;
        tanks.clear();
        walls.clear();
        missiles.clear();
        Runtime.getRuntime().gc();
    }

    public void addMissile(Point position, int direction, int strength, Tank unit) {
        missiles.add(new Missile(position, direction, strength, unit));
    }

    public void addWall(Point position, boolean immortal) {
        walls.add(new Wall(position, immortal));
    }

    public void addTank(Point position, boolean user) {
        Tank tank = new Tank(position, user);
        tanks.add(tank);
        if (user) {
	  userTank = tank;
	}
    }

    public Tank getUserTank() {
        return userTank;
    }

    public static UnitsManager getInstance() {
        if (manager == null) manager = new UnitsManager();
        return manager;
    }

}
