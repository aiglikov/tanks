
// Game - main game class, container for Field and UnitsManager

import java.awt.*;
import java.awt.event.*;

class Game {
// Game instance
    private static Game game = null;

// Game state constants
    private static final int STATE_NOTSTARTED = 0;
    private static final int STATE_PLAYING = 1;
    private static final int STATE_PAUSE = 2;
    private static final int STATE_FINISHED_WIN = 3;
    private static final int STATE_FINISHED_DEFEAT = 4;

// Game state
    private int state = STATE_NOTSTARTED;

    private Game() {

        String Action = "creating game";
        Log.addRecord(Action + "...");

        Log.addRecord("  " + Action + " - success.");
    }

    public void startNewGame() {
        if (state != STATE_NOTSTARTED) stopGame();

        String Action = "starting new game";
        Log.addRecord(Action + "...");

        MapManager.getInstance().loadMap("Maps/Map1.dat");

        state = STATE_PLAYING;

        GameRunner.getInstance().startGame();

        Log.addRecord("  " + Action + " - success.");
    }

    public void pauseGame() {
        String Action = "pausing game";
        Log.addRecord(Action + "...");

        UnitsManager.getInstance().pause();

        state = STATE_PAUSE;

        GameRunner.getInstance().pauseGame();

        MainWindow.getInstance().repaint();

        Log.addRecord("  " + Action + " - success.");
    }

    public void resumeGame() {
        String Action = "resuming game";
        Log.addRecord(Action + "...");

        state = STATE_PLAYING;

        GameRunner.getInstance().resumeGame();

        Log.addRecord("  " + Action + " - success.");
    }

    public void stopGame() {
        String Action = "stopping game";
        Log.addRecord(Action + "...");

        state = STATE_NOTSTARTED;

        GameRunner.getInstance().stopGame();
        try {
            Thread.currentThread().sleep(100);
        } catch (InterruptedException e) {
        }
        UnitsManager.getInstance().stop();
        EffectsManager.getInstance().stop();

        MainWindow.getInstance().repaint();

        Log.addRecord("  " + Action + " - success.");
    }

    public void paint(Graphics g, int width, int height) {
        Image image;

        MainWindow wnd = MainWindow.getInstance();;

        switch (state) {

            case STATE_NOTSTARTED:
                g.drawImage(ImagesManager.getInstance().getTitleImage(), 0, 0, wnd);
                break;

            case STATE_PLAYING:

            // Clearing window
                g.setColor(Color.BLACK);
                g.fillRect(0, 0, width - 1, height - 1);

            // Drawing field
                MapManager map = MapManager.getInstance();
                int dx = (width - map.getWidth()) / 2;
                int dy = (height - map.getHeight()) / 2;
                g.translate(dx, dy);
                map.paint(g);

            // Drawing units
                UnitsManager.getInstance().paint(g);

            // Drawing effects
                EffectsManager.getInstance().paint(g);
                break;

            case STATE_PAUSE:
                image = ImagesManager.getInstance().getPauseImage();
                g.drawImage(image, (width - image.getWidth(wnd)) / 2, (height - image.getHeight(wnd)) / 2, wnd);
                break;

            case STATE_FINISHED_WIN:
                image = ImagesManager.getInstance().getWinImage();
                g.drawImage(image, (width - image.getWidth(wnd)) / 2, (height - image.getHeight(wnd)) / 2, wnd);
                break;

            case STATE_FINISHED_DEFEAT:
                image = ImagesManager.getInstance().getDefeatImage();
                g.drawImage(image, (width - image.getWidth(wnd)) / 2, (height - image.getHeight(wnd)) / 2, wnd);
                break;

        }
    }

    public boolean processKeyPress(int key) {
        switch (key) {
            case KeyEvent.VK_ENTER:
                if (state != STATE_NOTSTARTED) return false;
                startNewGame();
                return true;

            case KeyEvent.VK_ESCAPE:
                if (state == STATE_PAUSE) resumeGame();
                else if (state == STATE_PLAYING) pauseGame();
                return true;

            case KeyEvent.VK_UP: case KeyEvent.VK_DOWN:
            case KeyEvent.VK_LEFT: case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_CONTROL:
                if (state != STATE_PLAYING) return false;
                UnitsManager.getInstance().processKeyPress(key);
                return true;

            default:
                return false;
        }
    }

    public boolean processKeyRelease(int key) {
        switch (key) {
            case KeyEvent.VK_UP: case KeyEvent.VK_DOWN:
            case KeyEvent.VK_LEFT: case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_CONTROL:
                if (state != STATE_PLAYING) return false;
                UnitsManager.getInstance().processKeyRelease(key);
                return true;

            default:
                return false;
        }
    }

    public void play() {
        UnitsManager.getInstance().play();
    }

    public void finish(boolean userWin) {
        GameRunner.getInstance().stopGame();
        UnitsManager.getInstance().pause();
        if (userWin) state = STATE_FINISHED_WIN;
        else state = STATE_FINISHED_DEFEAT;
        MainWindow.getInstance().repaint();
    }

    public boolean isFinished() {
        return (state == STATE_FINISHED_WIN) || (state == STATE_FINISHED_DEFEAT);
    }

    public static Game getInstance() {
        if (game == null) game = new Game();
        return game;
    }
}
