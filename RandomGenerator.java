import java.util.*;

class RandomGenerator {

    private static Random random = null;

    public static int nextInt(int n) {
        if (random == null) random = new Random(new Date().getTime());
        return random.nextInt(n);
    }

}
