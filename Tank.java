
// Tank - implements tank

import java.awt.*;
import java.util.*;

class Tank extends MovingShootingUnit {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final int HEALTH = 3;
    private static final int USERHEALTH = 4;

    private boolean moving;
    private boolean shooting;
    private boolean user;

    private Image[][] images;
    private int imageCount;
    private int imageIndex;
                             
    public Tank(Point pos, boolean user) {
//        String Action = "creating new tank";
//        Log.addRecord(Action + "...");

        destroyed = false;

        strength = 1;
        unlimitedAmmunition = true;
        ammunition = 1;
//        if (user) shootInterval = 40;
        shootInterval = 60;
        shootTimeout = 0;

        speed = 1;
        direction = 3;

        immortal = false;
        if (user) health = USERHEALTH;
        else health = HEALTH;
        position = new Point(pos);

        if (user) images = ImagesManager.getInstance().getUserTankImages();
        else images = ImagesManager.getInstance().getTankImages();

        moving = false;
        shooting = false;
        this.user = user;

        imageCount = ImagesManager.TANK_IMAGES_COUNT;
        imageIndex = 0;

        image = images[direction][imageIndex];

//        Log.addRecord("  " + Action + " - success.");
    }

    public void play() {
        if (user) {
            if (moving) {
                Point newpos = new Point(position);
                newpos.translate(SHIFT[direction].x * speed, SHIFT[direction].y * speed);
                if (UnitsManager.getInstance().getObstacle(this, newpos) == null) move();
            }
            if (shooting) shoot();
        } else {
            int d = MapManager.getInstance().getDirection(this);
            if (d == -1) {
                Point newpos = new Point(position);
                newpos.translate(SHIFT[direction].x * speed, SHIFT[direction].y * speed);
                Unit obstacle = UnitsManager.getInstance().getObstacle(this, newpos);
                if (obstacle != null) {
                    setDirection(RandomGenerator.nextInt(4));
                    newpos = new Point(position);
                    newpos.translate(SHIFT[direction].x * speed, SHIFT[direction].y * speed);
                    if (UnitsManager.getInstance().getObstacle(this, newpos) == null) move();
                } else if (RandomGenerator.nextInt(100) < 3) {
                    setDirection(RandomGenerator.nextInt(4));
                    newpos = new Point(position);
                    newpos.translate(SHIFT[direction].x * speed, SHIFT[direction].y * speed);
                    if (UnitsManager.getInstance().getObstacle(this, newpos) == null) move();
                } else move();
            } else {
                setDirection(d);
                int dist = MapManager.getInstance().getDistance(position);
                if (dist > 1) {
                    Point newpos = new Point(position);
                    newpos.translate(SHIFT[direction].x * speed, SHIFT[direction].y * speed);
                    if (UnitsManager.getInstance().getObstacle(this, newpos) == null) move();
                }
                if (MapManager.getInstance().see(this, UnitsManager.getInstance().getUserTank())) shoot();
            }
            if (RandomGenerator.nextInt(100) == 0) shoot();
        }
        if (shootTimeout > 0) --shootTimeout;
    }

    public void shoot() {
        if (shootTimeout > 0) return;
        shootTimeout = shootInterval;
        UnitsManager.getInstance().addMissile(position, direction, strength, this);
        SoundsManager.playShoot();
    }

    public void move() {
        super.move();
        ++imageIndex;
        if (imageIndex == imageCount) imageIndex = 0;
        image = images[direction][imageIndex];
    }

    protected void destroy() {
        super.destroy();
        EffectsManager.getInstance().addTankExplosion(position);
        SoundsManager.playExplosion();
	System.out.println("Tank destroyed");
    }

    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(ImagesManager.getInstance().getHealthImage(), position.x - WIDTH / 2, position.y + HEIGHT / 2 + 3, MainWindow.getInstance());
        g.setColor(Color.BLACK);
        int d;
        if (user) d = health * WIDTH / USERHEALTH;
        else d = health * WIDTH / HEALTH;
        g.fillRect(position.x - WIDTH / 2 + d, position.y + HEIGHT / 2 + 3, WIDTH - d, 5);
        g.setColor(Color.BLACK);
        g.drawRect(position.x - WIDTH / 2, position.y + HEIGHT / 2 + 3, WIDTH, 5);
    }

    public boolean isUser() {
        return user;
    }

    public void setUser(boolean value) {
        user = value;
    }

    public boolean isMoving() {
        return moving;
    }

    public void setMoving(boolean value) {
        moving = value;
    }

    public boolean getShooting() {
        return shooting;
    }

    public void setShooting(boolean value) {
        shooting = value;
    }

    public int getWidth() {
        return WIDTH;
    }

    public int getHeight() {
        return HEIGHT;
    }

    public void setDirection(int value) {
        image = images[value][imageIndex];
        super.setDirection(value);
    }

    public boolean canMove(int dir) {
        Point newpos = new Point(position);
        newpos.translate(SHIFT[dir].x * speed, SHIFT[dir].y * speed);
        Unit obstacle = UnitsManager.getInstance().getObstacle(this, newpos);
        return (obstacle == null) || (obstacle == UnitsManager.getInstance().getUserTank());
    }
}
