
// Main window class

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

class MainWindow extends Frame {

    private static MainWindow wnd = null;
    private static final int WIDTH = 800;
    private static final int HEIGHT = 600;
    private static final String TITLE = "Tanks";

    public Image image;
    public BufferStrategy buffer;
  
    private static ActionListener alNew = new ActionListener() {
        public void actionPerformed(ActionEvent event) {
            Game.getInstance().startNewGame();
        }
    };
  
    private static ActionListener alPause = new ActionListener() {
        public void actionPerformed(ActionEvent event) {
            Game.getInstance().pauseGame();
        }
    };
  
    private static ActionListener alStop = new ActionListener() {
        public void actionPerformed(ActionEvent event) {
            Game.getInstance().stopGame();
        }
    };
  
    private static ActionListener alExit = new ActionListener() {
        public void actionPerformed(ActionEvent event) {
            wnd.close();
        }
    };
  
    private static ActionListener alAbout = new ActionListener() {
        public void actionPerformed(ActionEvent event) {
            MessageBox.showMessage("This game was created by Artem Iglikov as a term work.", wnd, "About...");
        }
    };

    private MainWindow() {
        super(TITLE);

        String Action = "creating main window";
        Log.addRecord(Action + "...");

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0];
        GraphicsConfiguration gc = gd.getDefaultConfiguration();
        Rectangle bounds = gc.getBounds();
        setSize(WIDTH, HEIGHT);
        setLocation((bounds.width - WIDTH) / 2, (bounds.height - HEIGHT) / 2);

        setResizable(false);

        MenuBar mb = new MenuBar();
        Menu menu;
        MenuItem menuItem;

        menu = new Menu("Game");
        menuItem = new MenuItem("New");
        menuItem.addActionListener(alNew);
        menu.add(menuItem);
        menu.addSeparator();
        menuItem = new MenuItem("Pause");
        menuItem.addActionListener(alPause);
        menu.add(menuItem);
        menuItem = new MenuItem("Stop");
        menuItem.addActionListener(alStop);
        menu.add(menuItem);
        menu.addSeparator();
        menuItem = new MenuItem("Exit");
        menuItem.addActionListener(alExit);
        menu.add(menuItem);
        mb.add(menu);

        menu = new Menu("Help");
        menuItem = new MenuItem("About");
        menuItem.addActionListener(alAbout);
        menu.add(menuItem);
        mb.add(menu);

        setMenuBar(mb);

        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        enableEvents(AWTEvent.PAINT_EVENT_MASK);
        enableEvents(AWTEvent.KEY_EVENT_MASK);

        Log.addRecord("  " + Action + " - success.");
    }

    private void close() {
        String Action = "closing main window";
        Log.addRecord(Action + "...");

        Game.getInstance().stopGame();
        dispose();

        Log.addRecord("  " + Action + " - success.");
        System.exit(0);
    }

    protected void processEvent(AWTEvent event) {
        switch (event.getID()) {
            case WindowEvent.WINDOW_CLOSING:
                close();
                break;
            case KeyEvent.KEY_PRESSED:
                Game.getInstance().processKeyPress(((KeyEvent)event).getKeyCode());
                break;
            case KeyEvent.KEY_RELEASED:
                Game.getInstance().processKeyRelease(((KeyEvent)event).getKeyCode());
                break;
            default:
                super.processEvent(event);
        }
    }

    public void prepareBuffer() {
        addNotify();
        createBufferStrategy(2);
        buffer = getBufferStrategy();
    }

    public void paint(Graphics g) {
        Insets insets = getInsets();
        Dimension size = getSize();
        Graphics gr = buffer.getDrawGraphics();
        gr.translate(insets.left, insets.top);
        Game.getInstance().paint(gr, size.width - insets.left - insets.right, size.height - insets.top - insets.bottom);
        buffer.show();
    }   

    public void update(Graphics g) {
        paint(g);
    }

    public static MainWindow getInstance() {
        if (wnd == null) wnd = new MainWindow();
        return wnd;
    }

}
