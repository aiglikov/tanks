
// Main application class

class Tanks {
    public static void main(String[] args) {
        Log.addRecord("Tanks");
        Log.addRecord("Created by Artem Iglikov as a term work in 2006.");
        Log.addRecord("");

        MainWindow.getInstance();
        Game.getInstance();
        MainWindow.getInstance().prepareBuffer();
        MainWindow.getInstance().setVisible(true);

    }
}
