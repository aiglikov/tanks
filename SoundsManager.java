
// SoundsManager - loads and contains all sounds

import java.io.*;
import javax.sound.sampled.*;

class Sound extends Thread {

    String fileName;

    public Sound(String fileName) {
        this.fileName = fileName;
    }

    public void run() {
        try {
            try {
                DataLine.Info info = new DataLine.Info(Clip.class, AudioSystem.getAudioFileFormat(new File(fileName)).getFormat());
                if (!AudioSystem.isLineSupported(info)) { }
                try {
                    Clip clip = (Clip)AudioSystem.getLine(info);
                    clip.open(AudioSystem.getAudioInputStream(new File(fileName)));
                    clip.start();
                } catch (LineUnavailableException e) {
                }
            } catch (IOException e) {
            }
        } catch (UnsupportedAudioFileException e) {
        }
    }

}

class Shoot extends Sound {
    public Shoot() {
        super("Sounds/Shoot.wav");
    }
}

class Explosion extends Sound {
    public Explosion() {
        super("Sounds/Explosion.wav");
    }
}

class SoundsManager {

    public static void playShoot() {
        new Shoot().start();
    }

    public static void playExplosion() {
        new Explosion().start();
    }

}
