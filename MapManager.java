
// MapManager - contains map processing methods

import java.awt.*;
import java.io.*;
import java.util.*;

class MapManager {

// MapManager instance
    private static MapManager manager = null;

// Map
    static char map[][];
       
// Paths
    static int path[][];

// Map square size in pixels
    public static final int SQUARESIZE = 30;

// Map height and width in squares
    private int n;
    private int m;

// Map height and width in pixels
    private int height;
    private int width;

// Map background image
    private Image image;

    private MapManager() {
    }

    public void loadMap(String MapFile) {
        String Action = "creating map manager";
        Log.addRecord(Action + "...");

        String Action2 = "reading file " + MapFile;
        Log.addRecord(Action2 + "...");

        try {

            Scanner in = new Scanner(new FileReader(MapFile));

            n = in.nextInt();
            m = in.nextInt();
            map = new char[n][m];
            path = new int[n][m];
            for (int i = 0; i < n; ++i) {
                String s = in.next();
                for (int j = 0; j < m; ++j) {
                    map[i][j] = 0;
                    if (s.charAt(j) == '%') {
                        map[i][j] = 1;
                        UnitsManager.getInstance().addWall(new Point(j * SQUARESIZE + SQUARESIZE / 2, i * SQUARESIZE + SQUARESIZE / 2), false);
                    } else if (s.charAt(j) == '@') {
                        map[i][j] = 2;
                        UnitsManager.getInstance().addWall(new Point(j * SQUARESIZE + SQUARESIZE / 2, i * SQUARESIZE + SQUARESIZE / 2), true);
                    } else if (s.charAt(j) == '#')
                        UnitsManager.getInstance().addTank(new Point(j * SQUARESIZE + SQUARESIZE / 2, i * SQUARESIZE + SQUARESIZE / 2), false);
                    else if (s.charAt(j) == '$')
                        UnitsManager.getInstance().addTank(new Point(j * SQUARESIZE + SQUARESIZE / 2, i * SQUARESIZE + SQUARESIZE / 2), true);
                }
            }

            image = MainWindow.getInstance().getToolkit().getImage("Maps/" + in.next());

            MediaTracker tracker = new MediaTracker(MainWindow.getInstance());
            tracker.addImage(image, 0);
            try {
                tracker.waitForAll();
            } catch (InterruptedException e) {
                Log.addRecord("  " + Action + " - failure.");
                System.exit(1);
            }

            in.close();

            Log.addRecord("  " + Action2 + " - success.");
        } catch (IOException e) {
            Log.addRecord("  " + Action2 + " - failure.");
        }

        height = n * SQUARESIZE;
        width = m * SQUARESIZE;

        Log.addRecord("  " + Action + " - success.");
    }

    public void paint(Graphics g) {
        g.setColor(Color.GRAY);
        g.fillRect(0, 0, width, height);
        g.setColor(Color.WHITE);

        g.drawImage(image, 0, 0, MainWindow.getInstance());
//        for (int i = 0, y = 0; i <= n; ++i, y += SQUARESIZE) g.drawLine(0, y, width, y);
//        for (int j = 0, x = 0; j <= m; ++j, x += SQUARESIZE) g.drawLine(x, 0, x, height);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void removeWall(Point p) {
        map[p.y / SQUARESIZE][p.x / SQUARESIZE] = 0;
    }

    public void generatePath(Point p) {
        for (int i = 0; i < n; ++i) for (int j = 0; j < m; ++j) path[i][j] = -1;
        LinkedList<Point> queue = new LinkedList<Point>();
        Point v = new Point(p.x / SQUARESIZE, p.y / SQUARESIZE);
        path[v.y][v.x] = 0;
        queue.addLast(v);
        while (!queue.isEmpty()) {  
            v = queue.removeFirst();
            if ((v.x > 0) && (path[v.y][v.x - 1] == -1) && (map[v.y][v.x - 1] == 0)) {
                path[v.y][v.x - 1] = path[v.y][v.x] + 1;
                queue.addLast(new Point(v.x - 1, v.y));
            }
            if ((v.x < m - 1) && (path[v.y][v.x + 1] == -1) && (map[v.y][v.x + 1] == 0)) {
                path[v.y][v.x + 1] = path[v.y][v.x] + 1;
                queue.addLast(new Point(v.x + 1, v.y));
            }
            if ((v.y > 0) && (path[v.y - 1][v.x] == -1) && (map[v.y - 1][v.x] == 0)) {
                path[v.y - 1][v.x] = path[v.y][v.x] + 1;
                queue.addLast(new Point(v.x, v.y - 1));
            }
            if ((v.y < n - 1) && (path[v.y + 1][v.x] == -1) && (map[v.y + 1][v.x] == 0)) {
                path[v.y + 1][v.x] = path[v.y][v.x] + 1;
                queue.addLast(new Point(v.x, v.y + 1));
            }
        }

//        for (int i = 0; i < n; ++i, System.out.println("")) for (int j = 0; j < m; ++j) System.out.print(path[i][j] + " ");
//        System.out.println("");
    }

    public int getDirection(Tank tank) {
        Point v = new Point(tank.getPosition());
        v.x /= SQUARESIZE; v.y /= SQUARESIZE;
        if ((v.y > 0) && (path[v.y - 1][v.x] != -1) && (path[v.y - 1][v.x] < path[v.y][v.x]) && tank.canMove(0)) return 0;
        if ((v.x > 0) && (path[v.y][v.x - 1] != -1) && (path[v.y][v.x - 1] < path[v.y][v.x]) && tank.canMove(1)) return 1;
        if ((v.y < n - 1) && (path[v.y + 1][v.x] != -1) && (path[v.y + 1][v.x] < path[v.y][v.x]) && tank.canMove(2)) return 2;
        if ((v.x < m - 1) && (path[v.y][v.x + 1] != -1) && (path[v.y][v.x + 1] < path[v.y][v.x]) && tank.canMove(3)) return 3;
        return -1;
    }

    public int getDistance(Point p) {
        return path[p.y / SQUARESIZE][p.x / SQUARESIZE];
    }

    public boolean see(Unit unit1, Unit unit2) {
        Point p1 = new Point(unit1.getPosition());
        p1.x /= SQUARESIZE; p1.y /= SQUARESIZE;
        Point p2 = new Point(unit2.getPosition());
        p2.x /= SQUARESIZE; p2.y /= SQUARESIZE;
        int dx = (p1.x < p2.x ? 1 : (p1.x > p2.x ? -1 : 0));
        int dy = (p1.y < p2.y ? 1 : (p1.y > p2.y ? -1 : 0));
        for (int x = p1.x, y = p1.y; (x != p2.x) || (y != p2.y); x += dx, y += dy)
            if (map[y][x] != 0) return false;
        return true;
    }

    public static MapManager getInstance() {
        if (manager == null) manager = new MapManager();
        return manager;
    }

}
