
// Unit - abstract superclass for all types of units

import java.awt.*;

abstract class Unit {

    protected Image image = null;

    protected boolean immortal;
    protected int health;
    protected Point position;
    protected boolean destroyed;

    public abstract int getWidth();
    public abstract int getHeight();

    public void paint(Graphics g) {
        g.drawImage(image, position.x - getWidth() / 2, position.y - getHeight() / 2, MainWindow.getInstance());
    }

    public void attack(int strength) {
        if (!immortal) {
            health -= strength;
            if (health <= 0) destroy();
        }
    }

    protected void destroy() {
        destroyed = true;
    }

    public boolean isImmortal() {
        return immortal;
    }

    public void setImmortal(boolean value) {
        immortal = value;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int value) {
        health = value;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point value) {
        position.x = value.x;
        position.y = value.y;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

}
