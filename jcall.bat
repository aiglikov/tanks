cls

@echo off

echo Compiling Log.java
javac Log.java
if errorlevel 1 exit
echo Compiling MessageBox.java
javac MessageBox.java
if errorlevel 1 exit
echo Compiling RandomGenerator.java
javac RandomGenerator.java
if errorlevel 1 exit

echo Compiling ImagesManager.java
javac ImagesManager.java
if errorlevel 1 exit
echo Compiling SoundsManager.java
javac SoundsManager.java
if errorlevel 1 exit
echo Compiling EffectsManager.java
javac EffectsManager.java
if errorlevel 1 exit

echo Compiling Unit.java
javac Unit.java
if errorlevel 1 exit
echo Compiling MovingUnit.java
javac MovingUnit.java
if errorlevel 1 exit
echo Compiling MovingShootingUnit.java
javac MovingShootingUnit.java
if errorlevel 1 exit
echo Compiling Tank.java
javac Tank.java
if errorlevel 1 exit
echo Compiling Missile.java
javac Missile.java
if errorlevel 1 exit
echo Compiling Wall.java
javac Wall.java
if errorlevel 1 exit

echo Compiling MapManager.java
javac MapManager.java
if errorlevel 1 exit
echo Compiling UnitsManager.java
javac UnitsManager.java
if errorlevel 1 exit
echo Compiling GameRunner.java
javac GameRunner.java
if errorlevel 1 exit
echo Compiling Game.java
javac Game.java
if errorlevel 1 exit
echo Compiling MainWindow.java
javac MainWindow.java
if errorlevel 1 exit

echo Compiling Tanks.java
javac Tanks.java
