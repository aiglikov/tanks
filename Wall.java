
// Wall - implements walls

import java.awt.*;

class Wall extends Unit {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;

    public Wall(Point pos, boolean immortal) {
//        String Action = "creating new wall";
//        Log.addRecord(Action + "...");

        destroyed = false;

        this.immortal = immortal;
        this.health = 4;
        position = new Point(pos);

        if (immortal) image = ImagesManager.getInstance().getImmortalWallImage();
        else image = ImagesManager.getInstance().getWallImage();

//        Log.addRecord("  " + Action + " - success.");
    }

    protected void destroy() {
        super.destroy();
        SoundsManager.playExplosion();
    }

    public int getWidth() {
        return WIDTH;
    }

    public int getHeight() {
        return HEIGHT;
    }

}
