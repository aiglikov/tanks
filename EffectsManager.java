import java.util.*;
import java.awt.*;

class TankExplosion {
    public static Image[] images = null;
    public static final int IMAGES_COUNT = 18;

    private static final int WIDTH = 60;
    private static final int HEIGHT = 60;

    private int index;
    private Point position;
    private int interval;

    private boolean finished;

    public TankExplosion(Point pos) {
        index = 0;
        interval = 5;
        finished = false;
        position = new Point(pos);
    }

    public void paint(Graphics g) {
        g.drawImage(images[index], position.x - WIDTH / 2, position.y - HEIGHT / 2, MainWindow.getInstance());
        if (--interval == 0) {
            ++index;
            interval = 5;
        }
        if (index == IMAGES_COUNT) finished = true;
    }

    public boolean isFinished() {
        return finished;
    }
}

class MissileExplosion {
    public static Image[] images = null;
    public static final int IMAGES_COUNT = 8;

    private static final int WIDTH = 20;
    private static final int HEIGHT = 20;

    private int index;
    private Point position;
    private int interval;

    private boolean finished;

    public MissileExplosion(Point pos) {
        index = 0;
        interval = 2;
        finished = false;
        position = new Point(pos);
    }

    public void paint(Graphics g) {
        g.drawImage(images[index], position.x - WIDTH / 2, position.y - HEIGHT / 2, MainWindow.getInstance());
        if (--interval == 0) {
            ++index;
            interval = 2;
        }
        if (index == IMAGES_COUNT) finished = true;
    }

    public boolean isFinished() {
        return finished;
    }
}

class EffectsManager {

    private static EffectsManager manager = null;

    private ArrayList<TankExplosion> tankExplosions;
    private ArrayList<MissileExplosion> missileExplosions;

    private EffectsManager() {
        String Action = "creating effects manager";
        Log.addRecord(Action + "...");

        Toolkit toolkit = MainWindow.getInstance().getToolkit();
        MediaTracker tracker = new MediaTracker(MainWindow.getInstance());
        TankExplosion.images = new Image[TankExplosion.IMAGES_COUNT];
        for (int i = 0; i < TankExplosion.IMAGES_COUNT; ++i) {
            TankExplosion.images[i] = toolkit.getImage("Images/TankExplosion_" + i + ".gif");
            tracker.addImage(TankExplosion.images[i], 0);
        }
        MissileExplosion.images = new Image[MissileExplosion.IMAGES_COUNT];
        for (int i = 0; i < MissileExplosion.IMAGES_COUNT; ++i) {
            MissileExplosion.images[i] = toolkit.getImage("Images/MissileExplosion_" + i + ".gif");
            tracker.addImage(MissileExplosion.images[i], 0);
        }
        try {
            tracker.waitForAll();
        } catch (InterruptedException e) {
            Log.addRecord("  " + Action + " - failure.");
            System.exit(1);
        }

        tankExplosions = new ArrayList<TankExplosion>();
        missileExplosions = new ArrayList<MissileExplosion>();

        Log.addRecord("  " + Action + " - success.");
    }

    public void paint(Graphics g) {
        if (!tankExplosions.isEmpty())
            for (Iterator i = tankExplosions.iterator(); i.hasNext(); ) {
                TankExplosion tankExplosion = (TankExplosion)i.next();
                if (tankExplosion.isFinished()) i.remove();
                else tankExplosion.paint(g);
            }
        if (!missileExplosions.isEmpty())
            for (Iterator i = missileExplosions.iterator(); i.hasNext(); ) {
                MissileExplosion missileExplosion = (MissileExplosion)i.next();
                if (missileExplosion.isFinished()) i.remove();
                else missileExplosion.paint(g);
            }
    }

    public void addTankExplosion(Point position) {
        tankExplosions.add(new TankExplosion(position));
    }

    public void addMissileExplosion(Point position) {
        missileExplosions.add(new MissileExplosion(position));
    }

    public void stop() {
        tankExplosions.clear();
        missileExplosions.clear();
    }

    public static EffectsManager getInstance() {
        if (manager == null) manager = new EffectsManager();
        return manager;
    }

}
