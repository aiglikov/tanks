
// ImagesManager - loads and contains all images

import java.awt.*;

class ImagesManager {

    private static ImagesManager manager;

    public static int TANK_IMAGES_COUNT = 6;

    private static Image title;
    private static Image[][] userTank;
    private static Image[][] tank;
    private static Image[] missile;
    private static Image wall;
    private static Image immortalWall;
    private static Image health;
    private static Image pause;
    private static Image win;
    private static Image defeat;

    private ImagesManager() {
        String Action = "Creating ImagesManager";
        Log.addRecord(Action + "...");

        Toolkit toolkit = MainWindow.getInstance().getToolkit();
        title = toolkit.getImage("Images/Title.jpg");
        userTank = new Image[4][TANK_IMAGES_COUNT];
        tank = new Image[4][TANK_IMAGES_COUNT];
        for (int i = 0; i < 4; ++i) for (int j = 0; j < TANK_IMAGES_COUNT; ++j) {
            userTank[i][j] = toolkit.getImage("Images/Tank1_" + new Integer(i + 1).toString() + new Integer(j + 1).toString() + ".gif");
            tank[i][j] = toolkit.getImage("Images/Tank2_" + new Integer(i + 1).toString() + new Integer(j + 1).toString() + ".gif");
        }
        missile = new Image[4];
        for (int i = 0; i < 4; ++i)
            missile[i] = toolkit.getImage("Images/Missile" + (i + 1) + ".gif");
        wall = toolkit.getImage("Images/Wall1.gif");
        immortalWall = toolkit.getImage("Images/Wall2.gif");
        health = toolkit.getImage("Images/Health.gif");
        pause = toolkit.getImage("Images/Pause.gif");
        win = toolkit.getImage("Images/Win.gif");
        defeat = toolkit.getImage("Images/Defeat.gif");

        MediaTracker tracker = new MediaTracker(MainWindow.getInstance());
        tracker.addImage(title, 0);
        for (int i = 0; i < 4; ++i) for (int j = 0; j < TANK_IMAGES_COUNT; ++j) {
            tracker.addImage(userTank[i][j], 0);
            tracker.addImage(tank[i][j], 0);
        }
        for (int i = 0; i < 4; ++i) tracker.addImage(missile[i], 0);
        tracker.addImage(wall, 0);
        tracker.addImage(immortalWall, 0);
        tracker.addImage(health, 0);
        tracker.addImage(pause, 0);
        tracker.addImage(win, 0);
        tracker.addImage(defeat, 0);
        try {
            tracker.waitForAll();
        } catch (InterruptedException e) {
            Log.addRecord("  " + Action + " - failure.");
            System.exit(1);
        }

        Log.addRecord("  " + Action + " - success.");
    }

    public Image getTitleImage() {
        return title;
    }

    public Image[][] getUserTankImages() {
        return userTank;
    }

    public Image[][] getTankImages() {
        return tank;
    }

    public Image[] getMissileImages() {
        return missile;
    }

    public Image getWallImage() {
        return wall;
    }

    public Image getImmortalWallImage() {
        return immortalWall;
    }

    public Image getHealthImage() {
        return health;
    }

    public Image getPauseImage() {
        return pause;
    }

    public Image getWinImage() {
        return win;
    }

    public Image getDefeatImage() {
        return defeat;
    }

    public static ImagesManager getInstance() {
        if (manager == null) manager = new ImagesManager();
        return manager;
    }

}

