
// Missile - implements missiles

import java.awt.*;

class Missile extends MovingUnit {

    private static final int WIDTH = 6;
    private static final int HEIGHT = 15;

    private int strength;
    Tank owner;
    private Image[] images;

    public Missile(Point pos, int direction, int strength, Tank unit) {
//        String Action = "creating new missile";
//        Log.addRecord(Action + "...");

        destroyed = false;

        this.strength = strength;

        speed = 5;
        this.direction = direction;

        immortal = true;
        health = 1;
        position = new Point(pos);

        images = ImagesManager.getInstance().getMissileImages();
        image = images[direction];

        owner = unit;

//        Log.addRecord("  " + Action + " - success.");
    }

    public void play() {
        Point newpos = new Point(position);
        newpos.translate(SHIFT[direction].x * speed, SHIFT[direction].y * speed);
        Unit target = UnitsManager.getInstance().getObstacle(this, newpos);
        if (target == null) move();
        else {
            if (!(target instanceof Tank) || (((Tank)target).isUser() != owner.isUser())) target.attack(strength);
            if ((direction == 0) || (direction == 2))
                position.translate(0, SHIFT[direction].y * Math.abs((target.getPosition().y - position.y) / 2));
            else
                position.translate(SHIFT[direction].x * Math.abs((target.getPosition().x - position.x) / 2), 0);
            destroy();
        }
    }

    protected void destroy() {
        super.destroy();
        EffectsManager.getInstance().addMissileExplosion(position);
    }

    public int getWidth() {
        return ((direction == 0) || (direction == 2)) ? WIDTH : HEIGHT;
    }

    public int getHeight() {
        return ((direction == 0) || (direction == 2)) ? HEIGHT : WIDTH;
    }

    public void setDirection(int value) {
        super.setDirection(value);
        image = images[value];
    }

}
