class GameRunner extends Thread {

    private static GameRunner runner = null;

    private boolean playing = false;
    private boolean paused = false;

    public void startGame() {
        playing = true;
        paused = false;
        start();
    }

    public void stopGame() {
        playing = false;
        runner = null;
    }

    public void pauseGame() {
        paused = true;
    }

    public synchronized void resumeGame() {
        paused = false;
        notify();
    }

    public void run() {
        while (playing) {
            MainWindow.getInstance().update(MainWindow.getInstance().getGraphics());
            Game.getInstance().play();
            try {
                sleep(10);
                synchronized(this) {
                    while (paused) {
                        wait();
                    }
                }
            } catch (InterruptedException e) {
            }
//            System.out.println(Runtime.getRuntime().freeMemory());
        }
    }

    public static GameRunner getInstance() {
        if (runner == null) runner = new GameRunner();
        return runner;
    }

}
