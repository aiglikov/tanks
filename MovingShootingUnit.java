
// MovingShootingUnit - abstract superclass for moving and shooting units

abstract class MovingShootingUnit extends MovingUnit {

    protected int strength;
    protected boolean unlimitedAmmunition;
    protected int ammunition;
    protected int shootInterval;
    protected int shootTimeout;

    public abstract void shoot();

    public int getStrength() {
        return strength;
    }

    public void setStrength(int value) {
        strength = value;
    }

    public boolean isUnlimitedAmmunition() {
        return unlimitedAmmunition;
    }

    public void setUnlimitedAmmunition(boolean value) {
        unlimitedAmmunition = value;
    }

    public int getAmmunition() {
        return ammunition;
    }

    public void setAmmunition(int value) {
        ammunition = value;
    }

    public int getShootInterval() {
        return shootInterval;
    }

    public void setShootInterval(int value) {
        shootInterval = value;
    }

}
