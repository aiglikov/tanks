
// Auxiliary class for simple dialogs

import java.awt.*;
import java.awt.event.*;

class MessageBox extends Dialog {
    private static MessageBox wnd = null;

    private static ActionListener alExit = new ActionListener() {
        public void actionPerformed(ActionEvent event) {
            wnd.close();
        }
    };
  
    private MessageBox(String message, Frame parent, String title) {
        super(parent, title, true);

        String Action = "creating about box";
        Log.addRecord(Action + "...");

        setLayout(new GridLayout(2, 1));
        add(new Label(message, Label.CENTER));
        Panel panel = new Panel();
        Button button = new Button("   OK   ");
        button.addActionListener(alExit);
        panel.add(button);
        add(panel);
        pack();

        Point p = parent.getLocation();
        setLocation(p.x + (parent.getWidth() - getWidth()) / 2, p.y + (parent.getHeight() - getHeight()) / 2);

        enableEvents(AWTEvent.WINDOW_EVENT_MASK);

        Log.addRecord("  " + Action + " - success.");
    }

    private void close() {
        String Action = "closing about box";
        Log.addRecord(Action + "...");

        dispose();

        Log.addRecord("  " + Action + " - success.");
    }

    protected void processEvent(AWTEvent event) {
        if(event.getID() == WindowEvent.WINDOW_CLOSING) close();
        else super.processEvent(event);
    }

    public static void showMessage(String message, Frame parent, String title) {
        wnd = new MessageBox(message, parent, title);
        wnd.setVisible(true);
    }

}