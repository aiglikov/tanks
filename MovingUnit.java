
// MovingUnit - abstract superclass for all moving units

import java.awt.*;

abstract class MovingUnit extends Unit {

    protected static final Point[] SHIFT = { new Point(0, -1), new Point(-1, 0), new Point(0, 1), new Point(1, 0) };

    protected int speed;
    protected int direction;

    public abstract void play();

    public void move() {
        position.translate(SHIFT[direction].x * speed, SHIFT[direction].y * speed);
    }

    public int getMovingSpeed() {
        return speed;
    }

    public void setMovingSpeed(int value) {
        speed = value;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int value) {
        direction = value;
    }

}
